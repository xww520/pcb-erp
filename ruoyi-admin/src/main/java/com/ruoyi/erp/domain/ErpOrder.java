package com.ruoyi.erp.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 erp_order
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNo;

    /** 创建类型 */
    @Excel(name = "创建类型")
    private String createType;

    /** 销售员 */
    @Excel(name = "销售员")
    private String salesmanNo;

    /** 客户pcb源文件 */
    @Excel(name = "客户pcb源文件")
    private String pcbFile;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String customerNo;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String pactNo;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 下单数量 */
    @Excel(name = "下单数量")
    private Long pcsNum;

    /** 预报价 */
    @Excel(name = "预报价")
    private BigDecimal prePrice;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date orderTime;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String orderState;

    /** 工序状态 */
    @Excel(name = "工序状态")
    private String processState;

    /** 结算方式 */
    @Excel(name = "结算方式")
    private String settleWay;

    /** 收款状态 */
    @Excel(name = "收款状态")
    private String receiptState;

    /** 已收款金额 */
    @Excel(name = "已收款金额")
    private BigDecimal hasReceiptCash;

    /** 应收款金额 */
    @Excel(name = "应收款金额")
    private BigDecimal shouldReceiptCash;

    /** 欠款金额 */
    @Excel(name = "欠款金额")
    private BigDecimal debtCash;

    /** 交货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryDate;

    /** 包装方式 */
    @Excel(name = "包装方式")
    private String packStyle;

    /** 出货 */
    @Excel(name = "出货")
    private String shipType;

    /** 是否新单 */
    @Excel(name = "是否新单")
    private String isNewOrder;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setCreateType(String createType) 
    {
        this.createType = createType;
    }

    public String getCreateType() 
    {
        return createType;
    }
    public void setSalesmanNo(String salesmanNo) 
    {
        this.salesmanNo = salesmanNo;
    }

    public String getSalesmanNo() 
    {
        return salesmanNo;
    }
    public void setPcbFile(String pcbFile) 
    {
        this.pcbFile = pcbFile;
    }

    public String getPcbFile() 
    {
        return pcbFile;
    }
    public void setCustomerNo(String customerNo) 
    {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() 
    {
        return customerNo;
    }
    public void setPactNo(String pactNo) 
    {
        this.pactNo = pactNo;
    }

    public String getPactNo() 
    {
        return pactNo;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setPcsNum(Long pcsNum) 
    {
        this.pcsNum = pcsNum;
    }

    public Long getPcsNum() 
    {
        return pcsNum;
    }
    public void setPrePrice(BigDecimal prePrice) 
    {
        this.prePrice = prePrice;
    }

    public BigDecimal getPrePrice() 
    {
        return prePrice;
    }
    public void setOrderPrice(BigDecimal orderPrice) 
    {
        this.orderPrice = orderPrice;
    }

    public BigDecimal getOrderPrice() 
    {
        return orderPrice;
    }
    public void setOrderTime(Date orderTime) 
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime() 
    {
        return orderTime;
    }
    public void setOrderState(String orderState) 
    {
        this.orderState = orderState;
    }

    public String getOrderState() 
    {
        return orderState;
    }
    public void setProcessState(String processState) 
    {
        this.processState = processState;
    }

    public String getProcessState() 
    {
        return processState;
    }
    public void setSettleWay(String settleWay) 
    {
        this.settleWay = settleWay;
    }

    public String getSettleWay() 
    {
        return settleWay;
    }
    public void setReceiptState(String receiptState) 
    {
        this.receiptState = receiptState;
    }

    public String getReceiptState() 
    {
        return receiptState;
    }
    public void setHasReceiptCash(BigDecimal hasReceiptCash) 
    {
        this.hasReceiptCash = hasReceiptCash;
    }

    public BigDecimal getHasReceiptCash() 
    {
        return hasReceiptCash;
    }
    public void setShouldReceiptCash(BigDecimal shouldReceiptCash) 
    {
        this.shouldReceiptCash = shouldReceiptCash;
    }

    public BigDecimal getShouldReceiptCash() 
    {
        return shouldReceiptCash;
    }
    public void setDebtCash(BigDecimal debtCash) 
    {
        this.debtCash = debtCash;
    }

    public BigDecimal getDebtCash() 
    {
        return debtCash;
    }
    public void setDeliveryDate(Date deliveryDate) 
    {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() 
    {
        return deliveryDate;
    }
    public void setPackStyle(String packStyle) 
    {
        this.packStyle = packStyle;
    }

    public String getPackStyle() 
    {
        return packStyle;
    }
    public void setShipType(String shipType) 
    {
        this.shipType = shipType;
    }

    public String getShipType() 
    {
        return shipType;
    }
    public void setIsNewOrder(String isNewOrder) 
    {
        this.isNewOrder = isNewOrder;
    }

    public String getIsNewOrder() 
    {
        return isNewOrder;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderNo", getOrderNo())
            .append("createType", getCreateType())
            .append("salesmanNo", getSalesmanNo())
            .append("pcbFile", getPcbFile())
            .append("customerNo", getCustomerNo())
            .append("pactNo", getPactNo())
            .append("modeNo", getModeNo())
            .append("pcsNum", getPcsNum())
            .append("prePrice", getPrePrice())
            .append("orderPrice", getOrderPrice())
            .append("orderTime", getOrderTime())
            .append("orderState", getOrderState())
            .append("processState", getProcessState())
            .append("settleWay", getSettleWay())
            .append("receiptState", getReceiptState())
            .append("hasReceiptCash", getHasReceiptCash())
            .append("shouldReceiptCash", getShouldReceiptCash())
            .append("debtCash", getDebtCash())
            .append("deliveryDate", getDeliveryDate())
            .append("packStyle", getPackStyle())
            .append("shipType", getShipType())
            .append("isNewOrder", getIsNewOrder())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
