package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpAfterFeedbackMapper;
import com.ruoyi.erp.domain.ErpAfterFeedback;
import com.ruoyi.erp.service.IErpAfterFeedbackService;
import com.ruoyi.common.core.text.Convert;

/**
 * 售后反馈Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpAfterFeedbackServiceImpl implements IErpAfterFeedbackService 
{
    @Autowired
    private ErpAfterFeedbackMapper erpAfterFeedbackMapper;

    /**
     * 查询售后反馈
     * 
     * @param id 售后反馈ID
     * @return 售后反馈
     */
    @Override
    public ErpAfterFeedback selectErpAfterFeedbackById(String id)
    {
        return erpAfterFeedbackMapper.selectErpAfterFeedbackById(id);
    }

    /**
     * 查询售后反馈列表
     * 
     * @param erpAfterFeedback 售后反馈
     * @return 售后反馈
     */
    @Override
    public List<ErpAfterFeedback> selectErpAfterFeedbackList(ErpAfterFeedback erpAfterFeedback)
    {
        return erpAfterFeedbackMapper.selectErpAfterFeedbackList(erpAfterFeedback);
    }

    /**
     * 新增售后反馈
     * 
     * @param erpAfterFeedback 售后反馈
     * @return 结果
     */
    @Override
    public int insertErpAfterFeedback(ErpAfterFeedback erpAfterFeedback)
    {
        erpAfterFeedback.setId(IdUtils.fastSimpleUUID());
        erpAfterFeedback.setCreateTime(DateUtils.getNowDate());
        return erpAfterFeedbackMapper.insertErpAfterFeedback(erpAfterFeedback);
    }

    /**
     * 修改售后反馈
     * 
     * @param erpAfterFeedback 售后反馈
     * @return 结果
     */
    @Override
    public int updateErpAfterFeedback(ErpAfterFeedback erpAfterFeedback)
    {
        erpAfterFeedback.setUpdateTime(DateUtils.getNowDate());
        return erpAfterFeedbackMapper.updateErpAfterFeedback(erpAfterFeedback);
    }

    /**
     * 删除售后反馈对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpAfterFeedbackByIds(String ids)
    {
        return erpAfterFeedbackMapper.deleteErpAfterFeedbackByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除售后反馈信息
     * 
     * @param id 售后反馈ID
     * @return 结果
     */
    @Override
    public int deleteErpAfterFeedbackById(String id)
    {
        return erpAfterFeedbackMapper.deleteErpAfterFeedbackById(id);
    }
}
